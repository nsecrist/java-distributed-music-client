package secrist.wsclient;

import cst420.media.*;

import secrist.ws.server.libraryws.*;
import secrist.ws.server.libraryfactory.*;
import secrist.ws.server.musicdescription.*;
import javax.swing.*;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.sound.sampled.*;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.ws.wsaddressing.W3CEndpointReference;
import java.io.*;
import java.net.*;
import java.net.URL;
import javax.swing.tree.*;
import javax.swing.event.*;
import javax.swing.text.html.*;
import java.awt.event.*;
import java.awt.*;
import java.util.*;
import java.util.concurrent.locks.*;

/**
 * Purpose: Demonstrate my understanding of seralization and threading
 * using a simple music player example project.
 *
 * The application has been updated to utilized JAX-WS and Tomcat to demonstrate
 * my understanding of distributed application programming.
 * 
 * @author Tim Lindquist (Tim.Lindquist@asu.edu), ASU Polytechnic, Engineering
 * @version August 2013
 * 
 * Adapted by:
 * @author Nathan Secrist (nsecrist@asu.edu), ASU Polytechnic, Engineering
 * @since October 2013
 */
public class MusicLibrary extends MusicLibraryGui implements
		TreeWillExpandListener, ActionListener, TreeSelectionListener {

	private static final boolean debugOn = true;
	private PlayWavThread player = null;
	private boolean stopPlaying;
	private ReadWriteLock pauseLock = new ReentrantReadWriteLock();

	/**
	 * MusicLibraryWS Objects
	 */
	private LibraryFactory factory;
	private LibraryWSImplService librarySvc;
	private LibraryWSImpl library;
	private MusicDescriptionService musicDescSvc;
	private MusicDescription musicDesc;

	private Vector<W3CEndpointReference> musicDescriptions;

	/**
	 * Constructor for the Music Library client.
	 *
	 * Calls MusicLibraryGui constructor and adds listeners for the buttons.
	 * Also creates WS objects.
	 */
	public MusicLibrary(String base) {
		super(base);
		try {
			for (int i = 0; i < userMenuItems.length; i++) {
				for (int j = 0; j < userMenuItems[i].length; j++) {
					userMenuItems[i][j].addActionListener(this);
				}
			}
			tree.addTreeSelectionListener(this);
			tree.addTreeWillExpandListener(this);
			pauseResJB.addActionListener(this);

			// Web service additions
			musicDescriptions = new Vector<W3CEndpointReference>();
			factory = 
				new LibraryFactoryService().getLibraryFactoryPort();
			librarySvc = new LibraryWSImplService();
			musicDescSvc = new MusicDescriptionService();
			library = librarySvc.getPort(factory.getALibrary(base),
				LibraryWSImpl.class);

			initializeTree();

			setVisible(true);
		} catch (Exception ex) {
			JOptionPane.showMessageDialog(this,
				"Exception: " + ex.getMessage());
		}
	}

	private void debug(String message) {
		if (debugOn)
			System.out.println("debug: " + message);
	}
	
	/**
	 * a method to be called by music playing threads to determine
	 * whether they should stop
	 */
	public boolean isPaused() {
		return stopPlaying;
	}

	/**
	 * create and initialize nodes in the JTree of the left pane.
	 * buildInitialTree is called by MusicLibraryGui to initialize the JTree.
	 * Classes that extend MusicLibraryGui should override this method to
	 * perform initialization actions specific to the extended class. The
	 * default functionality is to set base as the label of root. In your
	 * solution, you will probably want to initialize by deserializing your
	 * library and building the tree.
	 * 
	 * @param root
	 *            Is the root node of the tree to be initialized.
	 * @param base
	 *            Is the string that is the root node of the tree.
	 */
	public void buildInitialTree(DefaultMutableTreeNode root, String base) {
		try {
			debug("buildInitialTree called by Gui constructor");
		} catch (Exception ex) {
			JOptionPane.showMessageDialog(this,"exception initial tree:"+ ex);
			ex.printStackTrace();
		}
	}

	private void buildTree(DefaultMutableTreeNode root, 
		DefaultTreeModel model) {
		
		// Obtain the references from the Server before building
		// the tree.
		java.util.List<W3CEndpointReference> temp = 
			library.getMusicDescriptions();
		musicDescriptions = new Vector<W3CEndpointReference>(temp);

		int numOfSongs = musicDescriptions.size();
		String[] titles = new String[numOfSongs];
		ArrayList<String> albums = new ArrayList<String>();
		
		for (int i = 0; i < numOfSongs; i++) {
			MusicDescription tmp = musicDescSvc
				.getPort(musicDescriptions.get(i), MusicDescription.class);
			String tmpAlbum = tmp.getAlbum();
			String tmpTitle = tmp.getTitle();

			titles[i] = tmp.getTitle();
			if (!albums.contains(tmp.getAlbum())) {
				albums.add(tmp.getAlbum());
			}
		}

		// Create the album node array
		debug("Begin creating album node array.");
		DefaultMutableTreeNode[] albumNodes = 
			new DefaultMutableTreeNode[albums.size()];

			// Index starts at 0
			for (int index = 0; index < albums.size(); index++) {
				albumNodes[index] = new DefaultMutableTreeNode(
						albums.get(index));
			}

		debug("Begin creating title node array.");
		// Create the song title node array
		DefaultMutableTreeNode[] titleNodes = 
			new DefaultMutableTreeNode[titles.length];
		for (int index = 0; index < titleNodes.length; index++) {
			titleNodes[index] = new DefaultMutableTreeNode(titles[index]);
		}
		debug("End creating title node array.");
			
		debug("Size of Titles Array: " + titles.length);
		debug("Size of Titles Nodes Array: " + titleNodes.length);
		
		debug("Begin creating J tree.");
		// Construct the J tree from the album and title node arrays
		// Insert the album name as a child of root node
		for (int index = 0; index < albumNodes.length; index++) {
			model.insertNodeInto(albumNodes[index], root,
			model.getChildCount(root));

		// Find and insert song titles as children of current album node
		debug("Added album to J tree.");
			for (int j = 0; j < titles.length; j++) {

			MusicDescription tmp = musicDescSvc.getPort(
				library.findReference(titles[j]), MusicDescription.class);

				if (tmp.getAlbum().equals(albums.get(index))) {
						model.insertNodeInto(
						titleNodes[j],     // Node to add as child.
						albumNodes[index], // Parent of current node to add.
						model.getChildCount(albumNodes[index]));
						debug("Song added.");
				}
			}
			debug("Added Songs as Children of Album Node.");
		}
		debug("End creating J tree.");
			
		// expand all the nodes in the JTree
		for (int r = 0; r < tree.getRowCount(); r++) {
			tree.expandRow(r);
		}
	}

	public void initializeTree() {
		tree.removeTreeSelectionListener(this);
		tree.removeTreeWillExpandListener(this);
		try {
			DefaultTreeModel model = (DefaultTreeModel) tree.getModel();
			DefaultMutableTreeNode root = (DefaultMutableTreeNode) model
					.getRoot();
			String user = System.getProperty("user.name");
			debug("user name is: " + user);
			
			clearTree(root, model);

			buildTree(root, model);
			
		} catch (Exception ex) {
			JOptionPane.showMessageDialog(this,"exception initial tree:"+ ex);
			ex.printStackTrace();
		}
		tree.addTreeSelectionListener(this);
		tree.addTreeWillExpandListener(this);
	}

	public void treeWillCollapse(TreeExpansionEvent tee) {
		tree.setSelectionPath(tee.getPath());
	}

	public void treeWillExpand(TreeExpansionEvent tee) {
		DefaultMutableTreeNode dmtn = (DefaultMutableTreeNode) tee.getPath()
				.getLastPathComponent();
		debug("will expand node: " + dmtn.getUserObject()
				+ " whose path is: " + tee.getPath());
	}

	public void valueChanged(TreeSelectionEvent e) {
		try {
			tree.removeTreeSelectionListener(this);
			DefaultMutableTreeNode node = (DefaultMutableTreeNode) tree
					.getLastSelectedPathComponent();
			if (node.isLeaf() && !node.isRoot()) {
				String nodeLabel = (String) node.getUserObject();
				
				// Grab the MusicDescription reference from the server
				// to use in populating the JTFs
				W3CEndpointReference selectedSongRef = library
					.findReference(nodeLabel);
				MusicDescription selectedSong = musicDescSvc.getPort(
					selectedSongRef, MusicDescription.class);

				titleJTF.setText(selectedSong.getTitle());
				authorJTF.setText(selectedSong.getArtist());
				albumJTF.setText(selectedSong.getAlbum());
			}
			else if (node.isRoot()) {
				titleJTF.setText("");
				authorJTF.setText("");
				albumJTF.setText("");
			}
			else {
				titleJTF.setText("");
				authorJTF.setText("");
				albumJTF.setText("");
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		tree.addTreeSelectionListener(this);
	}


	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().equals("Exit")) {
			System.exit(0);
		} else if (e.getActionCommand().equals("Save")) {
			//NOT NEEDED FOR THIS ASSIGNMENT
			//NO LONGER IMPLEMENTED
			//lib.saveLibrary();
			debug("Save Selected");
			
			library.saveLibrary();

		} else if (e.getActionCommand().equals("Restore")) {
			//NOT NEEDED FOR THIS ASSIGNMENT
			//NO LONGER IMPLEMENTED
			//debug("Restore selected, initializing tree");
			//initializeTree();

		} else if (e.getActionCommand().equals("Tree Refresh")) {
			
			initializeTree();			

		} else if (e.getActionCommand().equals("Add")) {
			debug("Add Selected");
			try {

				JFileChooser chooser = new JFileChooser();
				debug("File chooser created");

				FileNameExtensionFilter filter = new FileNameExtensionFilter(
					"WAV Media Files", "wav");
				debug("FileNameExtensionFilter created");

				chooser.setFileFilter(filter);
				debug("File chooser filter set");
				
				int returnVal = chooser.showOpenDialog(this);

				if (returnVal == JFileChooser.APPROVE_OPTION) {
					File uploadMe = chooser.getSelectedFile();
					debug("File to be uploaded " + uploadMe.getName());
					
					UploadThread upload = new UploadThread(titleJTF.getText(), 
						uploadMe);
					upload.start();
				}


				W3CEndpointReference w3cepr = library.addMusicDescription(
					titleJTF.getText(), authorJTF.getText(), albumJTF.getText());
			
				musicDescriptions.add(w3cepr);

				//lib.saveLibrary();
				initializeTree();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		} else if (e.getActionCommand().equals("Remove")) {
			debug("Remove Selected");
			try{
				tree.removeTreeSelectionListener(this);
				DefaultMutableTreeNode node = (DefaultMutableTreeNode)
					tree.getLastSelectedPathComponent();
				
				String nodeLabel = (String) node.getUserObject();

				library.removeMusicDescription(nodeLabel);

				initializeTree();

			} catch (Exception ex) {
				ex.printStackTrace();
			}
		} else if (e.getActionCommand().equals("Play")) {
			
			// Authored by Tim Lindquist and adapted for use in
			// my program.
        
			try{
            debug("Play Selected");

            DefaultMutableTreeNode node = (DefaultMutableTreeNode)
               tree.getLastSelectedPathComponent();
               
						String nodeLabel = (String)node.getUserObject();

            if(player != null && player.isAlive()){
               debug("Already playing: Interrupting the thread");
               pauseLock.writeLock().lock();
               stopPlaying = true;
               Thread.sleep(500); // give the thread time to complete
               stopPlaying = false;
               pauseLock.writeLock().unlock();
            }
            
            // Create a new instance of the thread and start it
            player = new PlayWavThread(nodeLabel, this);
            player.start();
            }catch (InterruptedException ex){ // sleep may throw this exception
            debug("MusicThread sleep was interrupted.");
            ex.printStackTrace();
			}
		} else if (e.getActionCommand() == "Pause") {
			pauseLock.writeLock().lock();
			stopPlaying = true;
			pauseLock.writeLock().unlock();
			pauseResJB.setText("Resume");
			pauseResJB.setActionCommand("Resume");
			pauseResJB.updateUI();
				
		} else if (e.getActionCommand() == "Resume") {
			pauseLock.writeLock().lock();
			stopPlaying = false;
			pauseLock.writeLock().unlock();
			pauseResJB.setText("Pause");
			pauseResJB.setActionCommand("Pause");
			pauseResJB.updateUI();
		}
	}

	private void clearTree(DefaultMutableTreeNode root, DefaultTreeModel model) {
		tree.removeTreeSelectionListener(this);
		tree.removeTreeWillExpandListener(this);
		try {
			DefaultMutableTreeNode next = null;
			int subs = model.getChildCount(root);
			for (int k = subs - 1; k >= 0; k--) {
				next = (DefaultMutableTreeNode) model.getChild(root, k);
				debug("removing node labelled:" + (String) next.getUserObject());
				model.removeNodeFromParent(next);
			}
		} catch (Exception ex) {
			System.out.println("Exception while trying to clear tree:");
			ex.printStackTrace();
		}
		tree.addTreeSelectionListener(this);
		tree.addTreeWillExpandListener(this);
	}
	
	/**
	 * Used by external threads to push updates to the UI.
	 * 
	 * @param minFrames		Normally zero
	 * @param maxFrames 	Max number of frames contained int he .wav
	 * @param posFrame		Current position in the file, indicated by the JSlider
	 * 
	 * @param maxSeconds	Maximum length of the .wav in seconds
	 * @param posSecond		Current position in the file in seconds
	 * 
	 * @author Nathan Secrist
	 */	
	public void updateUI(int minFrames, long maxFrames, long posFrame,
							double maxSeconds, double posSecond) {
		
		timeJLBL.setText(timeFormat(posSecond) + "/" + timeFormat(maxSeconds));
		progressJS.setMaximum((int) maxFrames);
		progressJS.setMinimum((int) minFrames);
		progressJS.setValue((int) posFrame);
	}
	
	/**
	 * Method used to format the length of the song in seconds into
	 * minutes and seconds.
	 * 
	 * @param pSec The time in seconds to be formatted
	 * 
	 * @author Nathan Secrist
	 */
	public String timeFormat(double pSec) {
		String time = "";
		
		int minutes = (int) pSec / 60;
		int remainingSeconds = (int) pSec - (minutes * 60);
		
		if (remainingSeconds < 10) {
			String seconds = "0" + remainingSeconds;
			time = minutes + ":" + seconds;
		}
		else { 
			time = minutes + ":" + remainingSeconds;
		}
		return time;
	}

	public static void main(String args[]) {
		try {
			String name = "Music Library";
			if (args.length >= 1) {
				name = args[0];
			}
			MusicLibrary ltree = new MusicLibrary(name);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}

/**
 * This portion was adapted from a sample project at;
 * http://pooh.poly.asu.edu/Cst420/ClassNotes/Threads/cnThreads-14.html#0_pgfId-178722
 * @author Tim Lindquist
 **/
class PlayWavThread extends Thread {
	
	private boolean debugOn = true;
	
   private String aTitle;
   private MusicLibrary parent;
   public PlayWavThread(String aTitle, MusicLibrary parent) {
      this.parent = parent;
      this.aTitle = aTitle;
   }
   
   	private void debug(String message) {
		if (debugOn)
			System.out.println("debug: " + message);
	}

	public void run (){

		// Download and save .wav file
		
		// Temp file used for downloaded .wav
		File downFile = new File("downloaded.wav");
		Socket sock = null;

		try {
			
			// In the case that the temp already exists
			if (!downFile.exists()) {
				downFile.delete();
			}

			FileOutputStream fos = new FileOutputStream(downFile);
			sock = new Socket("localhost", 3030);

			// Tell the server we want to download a file
			StringBuilder command = new StringBuilder();
			command.append('D');
			command.append(aTitle);
			OutputStream outSock = sock.getOutputStream();
			InputStream inSock = sock.getInputStream();
			
			debug("Command: " + command);
			outSock.write(command.toString().getBytes());

			// Read from socket to save audio file from server
			byte serverOut[] = new byte[1024];
			int bytesRead = inSock.read(serverOut, 0, 1024);
			while (bytesRead != -1) {
				fos.write(serverOut, 0, bytesRead);
				bytesRead = inSock.read(serverOut, 0, 1024);
			}

			outSock.close();
			inSock.close();
			fos.close();
			sock.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

		int BUFFER_SIZE = 4096;
		AudioInputStream audioStream;
		AudioFormat audioFormat;
		SourceDataLine sourceLine;
		long frames = 0;
		long bytesProcessed = 0;
		long framesProcessed = 0;
		double currentPositionInSeconds = 0.0;
		
		try{
			Thread.sleep(200); //wait 200 milliseconds before playing the file.
			debug("Playing the wav file: " +aTitle);
			audioStream = AudioSystem.getAudioInputStream(downFile);
			audioFormat = audioStream.getFormat();
			
			frames = audioStream.getFrameLength();
			double durationInSeconds = (frames + 0.0) / audioFormat.getFrameRate();
			
			DataLine.Info i = new DataLine.Info(SourceDataLine.class, audioFormat);
			sourceLine = (SourceDataLine) AudioSystem.getLine(i);
			sourceLine.open(audioFormat);
			sourceLine.start();
			int nBytesRead = 0;
			byte[] abData = new byte[BUFFER_SIZE];
			
			while(nBytesRead != -1){
				try{
					if(!parent.isPaused()){
						nBytesRead = audioStream.read(abData, 0, abData.length);
						
					if (nBytesRead >= 0) {
						@SuppressWarnings("unused")
							int nBytesWritten = sourceLine.write(abData,0,nBytesRead);
					}
					
					// "Hack" mentioned to me by Chadd Ingersoll when 
					// mentioning to him that I was unable to figure 
					// out how to access the position field in the AudioInputStream
					bytesProcessed++;
					framesProcessed = (bytesProcessed * BUFFER_SIZE) / 2;
					currentPositionInSeconds = (framesProcessed + 0.0) / audioFormat.getFrameRate();
					}
					
				parent.updateUI(0, frames, framesProcessed, durationInSeconds, currentPositionInSeconds);
				
				} catch (Exception e){
				e.printStackTrace();
				}
			}
			sourceLine.drain();
			sourceLine.close();
			audioStream.close();
		}catch (Exception e){
			e.printStackTrace();
		}
	}
}

class UploadThread extends Thread {

	Socket sock;
	String songName;
	private boolean debugOn = true;
	File uploadMe;

	public UploadThread(String title, File upload) {
		this.songName = title;
		this.uploadMe = upload;
	}
	
   	private void debug(String message) {
		if (debugOn)
			System.out.println("debug: " + message);
	}

	public void run() {
		try {
			sock = new Socket("localhost", 3030);
			debug("Socket created at localhost:3030");

			// Have the File to be uploaded, turn it into a byte array
			FileInputStream fis = new FileInputStream(uploadMe);

			// Count available bytes in InputStream
			int count = fis.available();
			
			// Create OutputStream
			OutputStream outSock = sock.getOutputStream();
			debug("OutputStream created");

			// Tell server we want to upload a file
			StringBuilder command = new StringBuilder();
			command.append('U');
			command.append(songName);
			debug("command: " + command);

			outSock.write(command.toString().getBytes());

			DataInputStream dis = new DataInputStream(fis);
			
			// Create buffer to store bytes
			byte[] buffer = new byte[count];
			dis.readFully(buffer);
			outSock.write(buffer);
			
			outSock.close();
			dis.close();
			fis.close();
			sock.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
