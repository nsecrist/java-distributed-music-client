package secrist.ws.server;

import com.sun.xml.ws.developer.StatefulWebServiceManager;
import com.sun.xml.ws.developer.Stateful;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.xml.ws.wsaddressing.W3CEndpointReference;
import javax.xml.ws.soap.Addressing;
import java.util.*;
import java.io.*;
import java.beans.*;

/**
 * Purpose: Specifies the web interface for the client be able to interact
 * with the library.
 *
 * This also manages the MusicDescriptions, hence Library.class
 * 
 * @author Nathan Secrist (nsecrist@asu.edu), ASU Polytechnic, Engineering
 * @version October 2013
 */

@Stateful @WebService @Addressing
public class LibraryWSImpl {

	protected Vector<W3CEndpointReference> musicDescriptions;
	public String libraryName;

	public LibraryWSImpl() { }

	/**
	 * Library constructor which reads in the user's library.xml file
	 * and constructs the Library populating the Library.library
	 * ArrayList.
	 * 
	 * @param filePath
	 */
	public LibraryWSImpl(String pName) {
		musicDescriptions = new Vector<W3CEndpointReference>();
		this.libraryName = pName;
		//HttpServletRequest request = new HttpServletRequest();
		//loadLibrary(request.getServletContext().getRealPath("/") +
		//	"defaultLibrary.xml");
	}

	@WebMethod
	public String getLibraryName() {
		return libraryName;
	}
	
	/**
	 * Method to load the library from serialized XML.
	 * 
	 * 'library.xml'
	 * 
	 * NOT NEEDED FOR THIS ASSIGNMENT
	 *
	 * @param filePath
	 */
//	@WebMethod
//	private void loadLibrary(String filePath) {
//		try {
//			FileInputStream inFileStream = new FileInputStream(filePath);
//			XMLDecoder decoder = new XMLDecoder(inFileStream);
//			this.musicDescriptions = (Vector)decoder.readObject();
//			decoder.close();
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}

	/**
	 * Web method to creat a new music description and return a remote
	 * reference to the new music description.
	 * 
	 * @param pTitle
	 * @param pArtist 
	 * @param pAlbum
	 */
	@WebMethod
	public synchronized W3CEndpointReference addMusicDescription(String pTitle,
		String pArtist, String pAlbum) {

		MusicDescription mDesc = 
			new MusicDescription(pTitle, pArtist, pAlbum);
		
		// If the W3C object vector has not been created,
		// create it.
		if (musicDescriptions == null) {
			musicDescriptions = new Vector<W3CEndpointReference>();
		}	
		W3CEndpointReference ror = MusicDescription.manager.export(mDesc);
		musicDescriptions.add(ror);
		return ror;
	}

	/**
	 * Web method to remove a music description from the library.
	 * 
	 * @param pTitle - Title of the song to be removed
	 */
	@WebMethod
	public synchronized void removeMusicDescription(String pTitle) {
		if (musicDescriptions != null) {
			musicDescriptions.remove(findReference(pTitle));
		}
	}
	
	/**
	 * Public method for the Web Service and client to use in finding a 
	 * W3CReferenceEndpoint object by the title of the song.
	 *
	 * @param pTitle - Title of the song you want the reference of
	 */
	@WebMethod
	public synchronized W3CEndpointReference findReference(String pTitle) {
		MusicDescription temp = new MusicDescription();
		W3CEndpointReference returnable = MusicDescription.manager
			.export(temp);

		for (Enumeration<W3CEndpointReference> e =
			musicDescriptions.elements(); e.hasMoreElements();) {
			W3CEndpointReference ror =
				(W3CEndpointReference) e.nextElement();
			MusicDescription song = (MusicDescription) MusicDescription
				.manager.resolve(ror);
			if (song.songTitle.equals(pTitle)) {
				returnable = ror;
			}
		}
		return returnable;
	}
	
	/**
	 * web method to return a list of music description ror's
	 */
	@WebMethod
	public synchronized List<W3CEndpointReference> getMusicDescriptions() {
		List<W3CEndpointReference> retMe =
			(List<W3CEndpointReference>)new Vector<W3CEndpointReference>();

		if (musicDescriptions != null) {
			retMe = (List<W3CEndpointReference>)musicDescriptions;
		}
		return retMe;
	}

	/**
	 * Encodes the library array list into an XML file
	 *
	 * @Deprecated
	 */
//	@WebMethod
//	public synchronized void saveLibrary() {
//		try {
//			System.out.println("Saving User Library...");
//			FileOutputStream xmlos = 
//				new FileOutputStream(System.getenv("CATALINA_HOME") +
//				"/webapps/jaxws-musiclibrary/WEB-INF/defaultLibrary.xml");
//			XMLEncoder encoder = new XMLEncoder(xmlos);
//			encoder.writeObject(musicDescriptions);
//			encoder.close();
//			System.out.println(
//				"Exported library as xml to defaultLibrary.xml");
//			xmlos.close();
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
	

	/**
	 * The following two methods are important for the server in order
	 * for both clients to access the same library.
	 *
	 * Without them, the server does not know that the client already has a
	 * library and makes it a new one
	 */
	public boolean equals(Object that) {
		if (that == null || this.getClass() != that.getClass()) return false;
		return this.libraryName.equals(((LibraryWSImpl)that).libraryName);
	}

	public int hashCode() {
		return libraryName.hashCode();
	}

	// The manager is needed in order to obtain W3CReferenceEndpoints
	// from the client.
	public static StatefulWebServiceManager<LibraryWSImpl> manager;
}
