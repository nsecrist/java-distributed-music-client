package secrist.ws.server;

import com.sun.xml.ws.developer.StatefulWebServiceManager;
import javax.jws.WebService;
import javax.xml.ws.wsaddressing.W3CEndpointReference;

@WebService
public class LibraryFactory {
	
	/**
	 * Method for getting the reference id of the music library for
	 * the client.
	 */
	public W3CEndpointReference getALibrary(String id) {
		return LibraryWSImpl.manager.export(new LibraryWSImpl(id));
	}
}
