package secrist.ws.server;

import com.sun.xml.ws.developer.StatefulWebServiceManager;
import com.sun.xml.ws.developer.Stateful;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.xml.ws.soap.Addressing;
import java.util.*;
import java.io.*;

/**
 * A simple defenition of a song description to be stored in a List
 * @author Nathan Secrist
 * @see Library
 *
 * @since October 2013
 * 		Updated the class be a Web Service for JAX-WS deployed
 * 		to Apache Tomcat
 */
@Stateful @WebService @Addressing
public class MusicDescription implements Serializable{
	
	public String songTitle;	// The title of the song
	private String songArtist;	// The name of the artist
	private String albumTitle;	// Tht name of this songs album

	/**
	 * Create an empty MusicDescription object
	 */
	public MusicDescription()
	{
		songTitle = "";
		songArtist = "";
		albumTitle = "";
	}

	/**
	 * Create a new MusicDescription object with the provided data
	 * @param	title	The name of the song
	 * @param	artist	The name of the song's artist
	 * @param	album	The name of the album the song belongs to
	 */
	public MusicDescription(String pTitle, String pArtist, String pAlbum)
	{
		songTitle = pTitle;
		songArtist = pArtist;
		albumTitle = pAlbum;
	}
	
	@WebMethod
	public synchronized String getTitle() {
		return songTitle;
	}
	
	@WebMethod
	public synchronized String getArtist() {
		return songArtist;
	}
	
	@WebMethod
	public synchronized String getAlbum() {
		return albumTitle;
	}
	
	@WebMethod
	public synchronized void setTitle(String pTitle) {
		songTitle = pTitle;
	}
	@WebMethod
	public synchronized void setArtist(String pArtist) {
		songArtist = pArtist;
	}
	
	@WebMethod
	public synchronized void setAlbum(String pAlbum) {
		albumTitle = pAlbum;
	}
	

	// Rather than give this class it's own package, I opted to
	// create a method in library which will obtain the correct 
	// W3CReference for removing the MusicDescription from the library

	//public boolean equals(Object that) {
	//	if (that == null || this.getClass() != that.getClass()) return false;
	//	return this.songTitle.equals(((MusicDescription)that).songTitle);
	//}

	//public int hashCode() {
	//	return songTitle.hashCode();
	//}

	public static StatefulWebServiceManager<MusicDescription> manager;
}
