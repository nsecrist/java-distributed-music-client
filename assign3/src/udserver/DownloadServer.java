package secrist.udserver;

import java.net.*;
import java.io.*;
import java.util.*;

/**
 * A class to handle Up and Download of .wav files to and from the client
 *
 * @author Nathan Secrist (nsecrist@asu.edu), ASU Polytechnic, Engineering
 * @version November 2013
 */

public class DownloadServer extends Thread {
	
	private Socket con;
	private int id;

	private boolean debugOn = true;

	public DownloadServer (Socket sock, int id) {
		this.con = sock;
		this.id = id;
	}

	private void debug(String message) {
		if (debugOn)
			System.out.println("debug: " + message);
	}

	public void run() {
		try {
			InputStream inSock = con.getInputStream();
			debug("inSock created");
			
			OutputStream outSock = con.getOutputStream();
			debug("outSock created");

			byte buffer[] = new byte[1024];
			debug("buffer created");

			int bytesRead = inSock.read(buffer, 0, 1024);
			String clientRequest = new String(buffer, 0, bytesRead);

			if (clientRequest.charAt(0) == 'U') {
				// DO UPLOAD STUFF
				debug("Starting upload");

				// Get title of song
				String fileName = clientRequest.substring(1, bytesRead);
				debug("Writing file to: " + fileName);

				// Save the file here and name it songName
				FileOutputStream fos = new FileOutputStream("dataserver/" + fileName);
				debug("fos created");

				// Read song in bytes into a .wav file
				bytesRead = inSock.read(buffer, 0, 1024);
				debug("Begin reading song out of socket into buffer...");

				while (bytesRead != -1) {
					fos.write(buffer, 0, bytesRead);
					bytesRead = inSock.read(buffer, 0, 1024);
				}
				debug("Finished writing song to " + fileName);

				fos.close();
			
			}	else {
			//	Client must be requesting a download
				debug("Starting download");

				String fileName = new String(clientRequest.getBytes(), 1, bytesRead-1);
				debug("Reading from " + fileName);

				FileInputStream fis = new FileInputStream("dataserver/" + fileName);
				DataInputStream dis = new DataInputStream(fis);
				byte[] download = new byte[fis.available()];
				debug("Length of download in bytes: " + download.length);
				dis.readFully(download);
				outSock.write(download);
				
				dis.close();
				fis.close();
			}

			outSock.close();
			inSock.close();
			con.close();

		} catch (IOException e) {
			System.out.println("Unable to get I/O for the connection.");
			e.printStackTrace();
		}
	}

	public static void main (String args[]) {
		
		Socket sock;
		int id = 0;
		
		try {
			
			File serverDir = new File("dataserver");
			if (!serverDir.exists()) {
				boolean result = serverDir.mkdir();
			}

			ServerSocket serv = new ServerSocket(3030);
			
			while (true) {
				sock = serv.accept();
				DownloadServer myDownloadServer = 
					new DownloadServer(sock, id++);
				myDownloadServer.start();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
